package az.unibank.grpcserver;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class GrpcServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrpcServerApplication.class, args);
    }

//    @Bean
//    public Server grpcServer() throws IOException {
//        Server server = ServerBuilder.forPort(8080)
//                .addService(new HelloWorldService())
//                .build();
//        server.start();
//        return server;
//    }
}
