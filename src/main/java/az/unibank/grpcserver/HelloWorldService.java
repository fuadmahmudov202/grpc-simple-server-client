package az.unibank.grpcserver;

import com.example.grpc.HelloRequest;
import com.example.grpc.HelloResponse;
import com.example.grpc.HelloWorldServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class HelloWorldService extends HelloWorldServiceGrpc.HelloWorldServiceImplBase {
    @Override
    public void sayHello(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        String message = "Hello " + request.getName() + "!";
        HelloResponse response = HelloResponse.newBuilder().setMessage(message).setAdd("add value").build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
