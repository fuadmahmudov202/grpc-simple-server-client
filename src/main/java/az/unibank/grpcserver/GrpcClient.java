package az.unibank.grpcserver;

import com.example.grpc.HelloRequest;
import com.example.grpc.HelloResponse;
import com.example.grpc.HelloWorldServiceGrpc;
import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GrpcClient {
    private final HelloWorldServiceGrpc.HelloWorldServiceBlockingStub blockingStub;
    public GrpcClient(Channel channel) {
        blockingStub = HelloWorldServiceGrpc.newBlockingStub(channel);
    }

    public static void main(String[] args) {
        try {
            ManagedChannel channel = ManagedChannelBuilder
                    .forAddress("localhost", 9090)
                    .usePlaintext()
                    .build();
            GrpcClient client = new GrpcClient(channel);

            try {
                HelloResponse response = client.sayHello("World");
                System.out.println("Received message: " + response.getMessage());
                System.out.println("Received add: " + response.getAdd());

            } finally {
                channel.shutdownNow();
            }
        } catch (Exception e) {
            System.out.println("problem"+e);
        }
    }

    public HelloResponse sayHello(String name) {
        HelloRequest request = HelloRequest.newBuilder().setName(name).build();
        return blockingStub.sayHello(request);
    }
}
